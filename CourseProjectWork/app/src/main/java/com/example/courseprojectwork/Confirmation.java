package com.example.courseprojectwork;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Confirmation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rental_confirmation);

        if (getIntent().hasExtra("com.example.courseprojectwork.SOMETHING")) {
            TextView tv = (TextView) findViewById(R.id.textView2);
            String text = getIntent().getExtras().getString("com.example.courseprojectwork.SOMETHING");
            tv.setText(text);
        }
    }
}