﻿*** CourseProjectWork ***

This is a simple app for demonstrating a car rental mobile app ("Rent-A-Car").

The app was created in Windows environment using Android Studio 4.2.1 and the NEXUS 5X API29 x86 virtual device (emulator) with resolution 1080x1920:420dpi has been successfully used for running the app. Please keep these specs in mind if you run into problems while trying to run the project.

In order to run the project one should copy the CourseProjectWork folder including all its content to his/her pc and save it to e.g. AndroidStudioProjects folder (or any folder one uses for saving Android Studio projects), start the Android Studio and goto: File -> Open -> CourseProjectWork. After the project has been loaded, the app can be run e.g. by pressing Shift+F10.

The content and functionalities of the app itself are quite self-explanatory once the app is up and running but a short video presentation of how the app is working can be found here: https://vimeo.com/581605509
