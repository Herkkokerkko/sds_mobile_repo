package com.example.courseprojectwork;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);



        Button button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EditText editTextTextPersonName2 = (EditText) findViewById(R.id.editTextTextPersonName2);
                Editable name = editTextTextPersonName2.getText();

                EditText editTextTextPersonName3 = (EditText) findViewById(R.id.editTextTextPersonName3);
                Editable number = editTextTextPersonName3.getText();

                Intent startIntent = new Intent(getApplicationContext(), Confirmation.class);
                startIntent.putExtra("com.example.courseprojectwork.SOMETHING", "Your car rental has now been confirmed, " + name + "! " + "We will call your number " + number + " when the car is ready for your pick up from our garage at Wildman Street 2 C.");
                startActivity(startIntent);
            }
        });

        Intent in = getIntent();
        int index = in.getIntExtra("com.example.ITEM_INDEX", -1);

        if (index > -1) {
            int pic = getImg(index);
            ImageView img = (ImageView) findViewById(R.id.imageView);
            scaleImg(img, pic);
        }
    }

    private int getImg(int index) {
        switch (index) {
            case 0: return R.drawable.ferrari;
            case 1: return R.drawable.chevrolet;
            case 2: return R.drawable.lada;
            case 3: return R.drawable.donald;
            default: return -1;
        }
    }

    private void scaleImg(ImageView img, int pic) {

        Display screen = getWindowManager().getDefaultDisplay();
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), pic, options);

        int imgWidth = options.outWidth;
        int screenWidth = screen.getWidth();

        if (imgWidth > screenWidth) {
            int ratio = Math.round( (float)imgWidth / (float)screenWidth);
            options.inSampleSize = ratio;
        }

        options.inJustDecodeBounds = false;
        Bitmap scaledImg = BitmapFactory.decodeResource(getResources(), pic, options);
        img.setImageBitmap(scaledImg);

    }

}